-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 26, 2018 at 04:26 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `password` varchar(250) NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `created`) VALUES
(1, 'sowmya', 'mallipudi', 'mnsow@gmail.com', 'abc123', '2018-11-15'),
(2, 'leela', 'bandaru', 'leela@gmail.com', 'abc123', '2018-11-16'),
(3, 'nani', 'mallipudi', 'nani@gmail.com', '$2b$10$9X/IXN07CqxiTDkfajHSPeSBaVGKIY3DOVgHnLNUPOl4ezAGJUoBW', '2018-11-16'),
(4, 'NagaSowmya', 'mallipudi', 'sowmya@gmail.com', '$2b$10$3J6nBT9EW3ldxlCBulQzde49LAOwZfSXu8mzFe94VkWteI.PmBpAO', '2018-11-16'),
(5, 'Ramaleela', 'Bandaru', 'leela1@gmail.com', '$2b$10$vaSuHhFNDjaQS1/biUDjYu09FkLKlch0pMb1ID3Q7Xql0Bj4o2fR.', '2018-11-16');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
